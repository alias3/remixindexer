# Remix Indexer (Package)
An horizontally scaled bot used to record smart contract events/views and update an database
in real time with these data, using Graph Protocol - Graph Node.

This workspace/package is used to deploy the Graph Node.